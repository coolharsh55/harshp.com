<h2>Abstract</h2>

<p>Artificial Intelligence (AI) has become one of the most important and ubiquitous technologies across the world. On a daily basis, we interact with powerful AI-based technologies through our use of mobile phones, voice assistants, and even our cars. Despite the widespread adoption of AI, questions and concerns exist around the ethical use of these technologies and their potential to reconfigure our personal and working lives. The Science Foundation Ireland ADAPT Research Centre has developed the Citizens’ Think-Ins model of citizen-researcher dialogue. ADAPT’s Think-In series to date has focused specifically on AI and the role it increasingly plays in our lives, and its impact on culture and society. This white paper presents an analysis of the various discussions that took place within the Citizens’ Think-Ins series. The discussions are presented with specific reference to citizens and civic society, academia, industry, and policymakers, and provide concrete recommendations to each stakeholder group, to draw parallels between their requirements, and to encourage the periodic use of Citizens’ Think-Ins as part of a larger deliberative and participatory approach comprising all stakeholders.</p>

<blockquote>
	<p>Access the full report at <a href="https://doras.dcu.ie/27820/">https://doras.dcu.ie/27820/</a></p>
</blockquote>

<h2>Table of Contents</h2>
<table>
	<tbody>
		<tr><td>Executive Summary</td><td>4</td></tr>
		<tr><td>Introduction</td><td>6</td></tr>
		<tr><td>Context: The Need for Real Debate on the Role of AI in Irish Society</td><td>6</td></tr>
		<tr><td>Building Trust in AI</td><td>8</td></tr>
		<tr><td>Citizens’ Think-Ins as a Method for Public Engagement</td><td>9</td></tr>
		<tr><td>The ADAPT Citizens’ Think-In Format</td><td>10</td></tr>
		<tr><td>Key Objectives of a Citizens’ Think-In</td><td>11</td></tr>
		<tr><td>Co-creation with Stakeholders</td><td>12</td></tr>
		<tr><td>Citizens’ Think-In Themes</td><td>12</td></tr>
		<tr><td>Insights arising from Think-In Discussions</td><td>13</td></tr>
		<tr><td>Trust and Privacy</td><td>14</td></tr>
		<tr><td>Control and Decision Making</td><td>15</td></tr>
		<tr><td>Governance and Regulation</td><td>16</td></tr>
		<tr><td>Analysis of Participant Perspectives</td><td>17</td></tr>
		<tr><td>Think-In Outcomes</td><td>19</td></tr>
		<tr><td>Conclusions</td><td>20</td></tr>
		<tr><td>Recommendations for Stakeholders</td><td>20</td></tr>
		<tr><td>Citizens</td><td>20</td></tr>
		<tr><td>Academia</td><td>21</td></tr>
		<tr><td>Policymakers</td><td>21</td></tr>
		<tr><td>Industry</td><td>22</td></tr>
		<tr><td>Next Steps</td><td>23</td></tr>
		<tr><td>Works referenced</td><td>24</td></tr>
		<tr><td>Acknowledgement of Contributions</td><td>26</td></tr>
		<tr><td>Appendix</td><td>27</td></tr>
		<tr><td>Citizens’ Think-Ins events 2020 - 2021</td><td>27</td></tr>
	</tbody>
</table>

<h2>Executive Summary</h2>
<p>Artificial Intelligence (AI) has become one of the most important and ubiquitous
technologies across the world. On a daily basis, we interact with powerful AI-based
technologies through our use of mobile phones, voice assistants, and even our cars.
Despite the widespread adoption of AI, questions and concerns exist around the
ethical use of these technologies and their potential to reconfigure our personal and
working lives.</p>
<p>The <a href="https://www.gov.ie/en/publication/91f74-national-ai-strategy/">AI - Here for Good</a> strategy (2021) places people at the centre of AI-based
technological innovation and prioritises the role played by citizens and other
members of society in developing understanding and trust in the potential of AI. The
strategy also acknowledges the need to establish public trust in AI. However, to date,
there have been few attempts to engage the public with the “social and ethical
implications of deploying AI systems across the public and private sectors” (Kerr et al,
2020a). There is a clear and growing need for public dialogue initiatives that create
opportunities for discussions on STEM technology. The Creating Our Future Expert
Report (2022) indicates that new research engagement programmes should be
developed based on models of inclusive participatory and deliberative democracy (pg
62).</p>
<p>The Science Foundation Ireland ADAPT Research Centre has developed the Citizens’
Think-Ins model of citizen-researcher dialogue. ADAPT’s Think-In series to date has
focused specifically on AI and the role it increasingly plays in our lives, and its impact
on culture and society. External evaluation of the Citizens’ Think-Ins series revealed
very positive outcomes for both citizens and researchers who participated in the
series.</p>
<p>Over the course of 2020 and 2021, ADAPT’s Citizens’ Think-In series fostered a wide
range of discussions on topics related to AI, ethics and privacy. Participants at
Citizens’ Think-Ins in 2020 and 2021 took the opportunity to share their concerns,
raise issues, identify opportunities and potential drawbacks, and to ask questions
within the forum.</p>
<p>At a broad level, three major topics emerged through the public discussions. These
are:</p>
<ol>
	<li>Trust and Privacy</li>
	<li>Control and Decision Making</li>
	<li>Governance and Regulation</li>
</ol>

<p>This white paper presents an analysis of the various discussions that took place
within the Citizens’ Think-Ins series. The discussions are presented with specific
reference to citizens and civic society, academia, industry, and policymakers, and
provide concrete recommendations to each stakeholder group, to draw parallels
between their requirements, and to encourage the periodic use of Citizens’ Think-Ins
as part of a larger deliberative and participatory approach comprising all
stakeholders.</p>
<p>Overall, the Citizens’ Think-In series offers a glimpse into how stakeholder-focused
discussions raise important issues regarding current and future trends in AI. They
also emphasise that individuals and groups provide unique perspectives to the
challenges associated with particular domains. AI-focused Citizens’ Think-In
discussions specifically highlighted that the use of automation and technology has
broader concerns that go beyond accountability and trustworthiness - participants
also had questions regarding the privacy and security of such technologies and their
broader impact on culture and society. The Think-In approach provides stakeholders
with an alternative mechanism to identify such issues and concerns, as well as
providing a platform to trial what methods can be developed to not only resolve the
solution legally or technologically, but also to build trust in the process.</p>

<h2>Funding Acknowledgements</h2>
<p>The Citizens’ Think-In project was funded by the Science Foundation Ireland (SFI)
Discover Programme. The SFI Discover Programme supports public engagement and
educational initiatives relating to science, technology, engineering, and mathematics.
ADAPT, the SFI Research Centre for AI-Driven Digital Content Technology, is funded by
Science Foundation Ireland through the SFI Research Centres Programme under
Grant Agreement No. 13/RC/2106_P2. 
Harshvardhan J. Pandit is funded under the Irish Research Council Government of
Ireland Postdoctoral Fellowship Grant#GOIPD/2020/790</p>